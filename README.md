# How to use the app
This application analyses an image and tries to predict a human.
While running the project you can send a request to ``/v1/analyse`` and add images to the POST parameter ``image``.
This will return an output in JSON format based on the schema_dsi_predictions schema ``https://schema.arise-biodiversity.nl/dsi/v0/dsi_algorithm_predictions.schema.json``

If you want to run the system on CPU you have to configure this in ``/utils/config``. Set the device to CPU or GPU.

# installation

To run and install all requisites
## Docker

    docker compose up

This will build and run the docker container.

### Test
You can test the container with the following command: 
 -     cd tests
 -     pytest test_algorithm.py


## Install requisites to run files locally

 - Install cuda 11.6 or 11.7 **ONLY IF YOU HAVE AN NVIDIA GPU**
 - Install pytorch https://pytorch.org/ according to your system
 -     pip install -r ./requirements_local.txt

 -     python -m pip install 'git+https://github.com/facebookresearch/detectron2.git'

 ## Changelog

These are the changes made to make the repo run on Arise infrastructure:

- used `Path.mkdir()` to make sure the parent folders exist for the image to be writen to 
- algorithm descriptor
    - `port` field needs to be the dockers internal port (here, 5000)
    - `infer_path` field had to drop the first and last `/`
    - `infer_post_variable_name` was not correct (changed it in code from `image` -> `media`)
    - `docker` field needs to be a URL of a Docker image registered in registry.gitlab.com
- moved tempfile creation before the iteration of the files sent to `v1/analyse`