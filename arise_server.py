import datetime
import logging
from pathlib import Path

import numpy as np
import os
import tempfile
import flask
import redis
import matplotlib.pyplot as plt
from os.path import dirname

from models.ensemble.Ensemble import Ensemble
from typing import Dict, Any


def str2bool(value, raise_exc=False) -> bool:
    """
    Converts a string to boolean by matching it with some well known representations of bool.
    {"yes", "true", "t", "y", "1"} --> true
    {"no", "false", "f", "n", "0"} --> false
    If value is not one of these representations it returns False or raises a ValueError when raise_exc==True
    :param value: a string representing a boolean
    :param raise_exc: if True and unknown boolean string representation raises a ValueError
    :raises ValueError
    :return: a boolean
    """
    _true_set = {"yes", "true", "t", "y", "1"}
    _false_set = {"no", "false", "f", "n", "0"}
    result = False
    value_ = str(value).lower()
    if value_ in _true_set:
        result = True
    elif value_ in _false_set:
        result = False
    else:
        if raise_exc:
            raise ValueError(
                'Expected "{}"'.format('", "'.join(_true_set | _false_set))
            )
    return result


def get_logger(name: str, filename: str) -> logging.Logger:
    """
    Configures a logger with name 'name'. If the name is empty ('') then you can just use logging.info(..),
    else use logging.getLogger(name).info() or the returned logger object.
    Needs environment variable LOG_FOLDER to be set
    :param name:
    :param filename:
    :return: logging.Logger
    """
    log_folder = os.environ.get("LOG_FOLDER", os.getcwd())

    if not os.path.exists(log_folder):
        os.makedirs(log_folder)
    filename = os.path.join(log_folder, filename)
    logger_ = logging.getLogger(name)
    logger_.setLevel(logging.INFO)
    logger_.handlers = []
    formatter = logging.Formatter("%(asctime)s [%(name)s] %(message)s")
    file_logger = logging.FileHandler(filename)
    file_logger.setFormatter(formatter)
    file_logger.setLevel(logging.INFO)
    logger_.addHandler(file_logger)
    console = logging.StreamHandler()
    console.setFormatter(formatter)
    console.setLevel(logging.DEBUG)
    logger_.addHandler(console)

    return logger_


class ApiException(BaseException):
    """
    Base class for raising and catching API specific exceptions
    """

    pass


app = flask.Flask(__name__, static_url_path="", static_folder="static")
logger = get_logger("ARISE_GETI_MODEL_SERVER", os.getenv("LOG_PATH", __file__ + ".log"))
JsonType = Dict[str, Any]
cache = redis.Redis(host='redis', port=6379)
app.secret_key = 'humandetector'


@app.route("/v1/analyse", methods=["POST"])
def analyse():
    """
    Analyses an image

    Expects POST parameter 'image'
    """
    # get uploaded photos
    uploaded_files = flask.request.files.getlist("media")
    logger.info("Number of uploaded files: {}".format(len(uploaded_files)))
    if len(uploaded_files) < 1:
        raise ApiException("No images uploaded")

    # this is the json format for the output
    out_json = {
        "name": "https://schemas.arise-biodiversity.nl/dsi/multi-object-multi-image#sequence-one-prediction-per-region",
        # TODO: there is no public URL with the schema yet
        "generated_by": {
            "datetime": datetime.datetime.now().isoformat() + "Z",
            "version": os.getenv("ALGORITHM_VERSION", "algorithm version unknown"),
            "tag": os.getenv("ALGORITHM_TAG", "algorithm tag unknown"),
        },
        "media": [],
        "region_groups": [],
        "predictions": [],
    }

    # Call our model.
    ensemble = Ensemble(docker_env=True)

    with tempfile.TemporaryDirectory() as tmp_dir:
        for i, image_id in enumerate(uploaded_files):
            uploaded_file = uploaded_files[i]
            uploaded_name = uploaded_file.filename

            # ARISE output format
            media_id = os.path.splitext(os.path.split(uploaded_name)[-1])[0]
            out_json["media"].append({"filename": uploaded_name, "id": media_id})

            # write the uploaded file to temp dir
            image_path = str(Path(f"{tmp_dir}/{uploaded_name}"))
            # Path(tmp_dir).mkdir(parents=True, exist_ok=True)
            print(f"dirnaming image_path: {image_path}")
            Path(dirname(image_path)).mkdir(parents=True, exist_ok=True)
            print(f"writing to image_path: {image_path}")
            # image_path = os.path.join(tmp_dir, uploaded_name)
            with open(image_path, "wb") as f:
                f.write(uploaded_file.read())
            print(f"loading from image_path: {image_path}")
            im = plt.imread(image_path)

            # get the classified boxes from algorithm
            json_data = ensemble.arise_output(im, image_id)

            # Append the classified boxes to the json output
            out_json = append_boxes(out_json, media_id, json_data['boxes'])

    return flask.jsonify(out_json)


def append_boxes(out_json, media_id, boxes):
    """
    This function appends the classified boxes to the json output format and returns it
    :param out_json: current json_ouput
    :param media_id: id of an image
    :param boxes: array of classified boxes
    :return: json output
    """
    per_image_region_counter = 0

    for box in boxes:
        region_id = media_id + f"?region={per_image_region_counter}"

        out_json["region_groups"].append(
            {
                "id": region_id,
                # because individuals are not tracked (yet), we re-use the region_id as individual_id
                "individual_id": region_id,
                "regions": [
                    {
                        "media_id": media_id,
                        "box": {"x1": box[0], "y1": box[1], "x2": box[2],
                                "y2": box[3]},
                    }
                ],
            }
        )

        out_json["predictions"].append(
            {
                "region_group_id": region_id,
                "taxa": {
                    "type": "multiclass",
                    "items": [
                        {
                            "scientific_name": box[5],
                            "scientific_name_id": f"LITERAL:{box[5].upper()}",
                            "probability": np.round(box[4], 6).tolist(),
                        }
                    ],
                },
            }
        )

        per_image_region_counter += 1

    return out_json


if __name__ == "__main__":
    app.run(
        debug=str2bool(os.getenv("DEBUG")),
        host=os.getenv("HOST", "0.0.0.0"),
        port=int(os.getenv("PORT", "5000")),
        use_reloader=False,
    )
