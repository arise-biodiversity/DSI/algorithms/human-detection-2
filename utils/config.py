from enum import Enum


class Device(Enum):
    CPU = 'cpu'
    GPU = 'cuda:0'


DEVICE = Device.CPU.value
