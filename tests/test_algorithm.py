import unittest
import requests
import json
import jsonschema


class TestAlgorithm(unittest.TestCase):
    def test_algorithm_single(self):
        response = requests.post(
            "http://localhost:8000/v1/analyse",
            files={"media": open("data/test-img.jpg", 'rb')},
        )

        self.assertEqual(200, response.status_code)
        json_response = response.json()
        print(json.dumps(json_response, indent=2))
        json.dump(json_response, open("data/api_response.json", "w"), indent=2)
        num_detections = 2
        self.assertEqual(num_detections, len(json_response["region_groups"]))
        self.assertEqual(num_detections, len(json_response["predictions"]))
        self.assertEqual(1, len(json_response["media"]))
        schema = json.load(open("data/schema_dsi_predictions.json"))
        jsonschema.validate(
            json_response, schema, format_checker=jsonschema.FormatChecker()
        )
