import requests
from pathlib import Path

url = "http://127.0.0.1:6000/v1/analyse"

payload = {}
images = Path("data")

files = []
for image in images.glob("*.jpg"):
    image_name = str(image.name)
    image_open_read_binary = image.open("rb")
    files.append(('media', (image_name, image_open_read_binary, 'image/jpeg')))

headers = {}

response = requests.request("POST", url, headers=headers, data=payload, files=files)

print(response.text)
