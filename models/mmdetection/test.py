# Some basic setup:
import matplotlib.pyplot as plt
import csv
import pandas as pd
import os

from Mmdetection import Mmdetection


class Test:
    """This class tests the mmdetection model on the test images.
    It saves the results to a csv."""
    def __init__(self):
        self.model = Mmdetection()

    def predict(self):
        df = pd.read_csv("../../data/test/csv/twente_labels.csv")
        ground_truth = df["ground_truth"]
        id = df["mediaID"].str.strip() + '.jpg'
        data = []
        images = os.listdir("../../data/test/images/")

        # Remove the .gitignore file otherwise model will count it too.
        images.remove('.gitignore')

        # Loop through the images and get the human prediction
        for i, image_dir in enumerate(images):
            im = plt.imread("../../data/test/images/" + image_dir)
            human, human_score = self.model.contains_human(im)
            data.append([id[i], ground_truth[i], human, human_score])
            print("Contains human = {:>10} \t Human score = {:>20}".format(human, human_score))
        return data


# Write data to csv
def write_csv(data, filename):
    header = ['mediaID', 'ground_truth', 'human_detected', 'accuracy']

    with open(f'../../data/test/csv/{filename}', 'w', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)

        # write the header
        writer.writerow(header)

        # write multiple rows
        writer.writerows(data)


if __name__ == '__main__':
    model = Test()
    data = model.predict()
    write_csv(data=data, filename="mmdetection.csv")
