import mmcv
import os.path as osp

from mmcv import Config
from mmdet.apis import set_random_seed
from mmdet.datasets import build_dataset
from mmdet.models import build_detector
from mmdet.apis import train_detector
from utils import config


class Train:
    """This method trains the mmdetection model with a custom COCO dataset.
    The method needs a directory path for the train_images and a path to the coco_file"""
    def __init__(self, directory, coco_file):
        self.cfg = Config.fromfile('./configs/faster_rcnn/faster_rcnn_r50_caffe_fpn_mstrain_1x_coco.py')
        self.directory = directory
        self.coco_file = coco_file
        self.setup_config()

    def setup_config(self):
        # Modify dataset type and path
        dataset = "CocoDataset"
        root = "../../data/train"
        classes = ['person']
        self.cfg.dataset_type = dataset
        self.cfg.data_root = root

        # Amount of epochs.
        self.cfg.runner.max_epochs = 1

        self.cfg.data.test.type = dataset
        self.cfg.data.test.ann_file = self.coco_file
        self.cfg.data.test.img_prefix = self.directory
        self.cfg.data.test.classes = classes

        self.cfg.data.train.type = dataset
        self.cfg.data.train.ann_file = self.coco_file
        self.cfg.data.train.img_prefix = self.directory
        self.cfg.data.train.classes = classes

        self.cfg.data.val.type = dataset
        self.cfg.data.val.ann_file = self.coco_file
        self.cfg.data.val.img_prefix = self.directory
        self.cfg.data.val.classes = classes

        # modify num classes of the model in box head
        self.cfg.model.roi_head.bbox_head.num_classes = 1


        # If we need to finetune a model based on a pre-trained detector, we need to
        # use load_from to set the path of checkpoints.
        self.cfg.load_from = './models/faster_rcnn_r50_fpn_1x_coco_20200130-047c8118.pth'

        # Set up working dir to save files and logs.
        self.cfg.work_dir = '../../models/mmdetection/output'

        # The original learning rate (LR) is set for 8-GPU training.
        # We divide it by 8 since we only use one GPU.
        self.cfg.optimizer.lr = 0.02 / 8
        self.cfg.lr_config.warmup = None
        self.cfg.log_config.interval = 10

        # Change the evaluation metric since we use customized dataset.
        self.cfg.evaluation.metric = 'bbox'
        # We can set the evaluation interval to reduce the evaluation times
        self.cfg.evaluation.interval = 12
        # We can set the checkpoint saving interval to reduce the storage cost
        self.cfg.checkpoint_config.interval = 12

        # Set seed thus the results are more reproducible
        self.cfg.seed = 0
        set_random_seed(0, deterministic=False)

        # This enables CPU if DEVICE is set to CPU. Default = GPU
        if config.DEVICE == 'cpu':
            self.cfg.device = 'cpu'

        self.cfg.gpu_ids = range(1)

        # We can also use tensorboard to log the training process
        self.cfg.log_config.hooks = [
            dict(type='TextLoggerHook'),
            dict(type='TensorboardLoggerHook')]

        # We can initialize the logger for training and have a look
        # at the final config used for training
        print(f'Config:\n{self.cfg.pretty_text}')

    def fit(self):
        # Build dataset
        datasets = [build_dataset(self.cfg.data.train)]

        # Build the detector
        model = build_detector(self.cfg.model)
        # Add an attribute for visualization convenience
        model.CLASSES = datasets[0].CLASSES
        # Create work_dir
        mmcv.mkdir_or_exist(osp.abspath(self.cfg.work_dir))
        train_detector(model, datasets, self.cfg, distributed=False, validate=True)


if __name__ == '__main__':
    directory = "../../data/train/images"
    coco_file = "../../data/train/_annotations.coco.json"
    model = Train(directory=directory, coco_file=coco_file)
    model.fit()

