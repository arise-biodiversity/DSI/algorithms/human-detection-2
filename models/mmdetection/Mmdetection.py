# Some basic setup:
from collections import namedtuple
from mmdet.apis import init_detector, inference_detector
from mmdet.core import get_classes
from utils import config

import numpy as np


class Mmdetection:
    # In case of docker the path is different.
    def __init__(self, docker_env=False):
        if docker_env:
            self.config_file = './models/mmdetection/configs/faster_rcnn/faster_rcnn_r50_fpn_1x_coco.py'
            self.checkpoint_file = './models/mmdetection/models/faster_rcnn_r50_fpn_1x_coco_20200130-047c8118.pth'
        else:
            self.config_file = './configs/faster_rcnn/faster_rcnn_r50_fpn_1x_coco.py'
            self.checkpoint_file = './models/faster_rcnn_r50_fpn_1x_coco_20200130-047c8118.pth'

        self.model = init_detector(self.config_file, self.checkpoint_file, device=config.DEVICE)

    def contains_human(self, image):
        """This function returns the prediction in binary and the score of the prediction. It takes one image."""

        # Init contains_human and human_score.
        contains_human = 0
        human_score = 0

        # Run the image through mmdetection and retrieve the results.
        results = inference_detector(self.model, image)

        # Extract the boxes that have been assigned to the prediction.
        labels = [
            np.full(bbox.shape[0], i, dtype=np.int32)
            for i, bbox in enumerate(results)
        ]
        labels = np.concatenate(labels)
        bboxes = np.vstack(results)

        # If the confidence is higher than 30% put it in the array.
        labels_impt = np.where(bboxes[:, -1] > 0.3)[0]

        classes = get_classes("coco")
        labels_impt_list = [labels[i] for i in labels_impt]
        labels_class = [classes[i] for i in labels_impt_list]

        # If a human has been detected return the score and the binary prediction.
        if 'person' in labels_class:
            contains_human = True
            human_score = bboxes[:, -1][0]

        return int(contains_human), human_score

    def arise_output(self, image, image_id):
        """This method returns the output of detected person on image in the format of ARISE."""
        # Run the image through the model.
        results = inference_detector(self.model, image)

        # Extract the boxes that have been drawn around the predictions.
        labels = [
            np.full(bbox.shape[0], i, dtype=np.int32)
            for i, bbox in enumerate(results)
        ]
        labels = np.concatenate(labels)
        bboxes = np.vstack(results)

        # If the confidence is higher than 30% put it in the array.
        labels_impt = np.where(bboxes[:, -1] > 0.3)[0]

        classes = get_classes("coco")
        labels_impt_list = [labels[i] for i in labels_impt]
        labels_class = [classes[i] for i in labels_impt_list]
        boxes = []

        ClassifiedBox = namedtuple(
            "ClassifiedBox", ["x1", "y1", "x2", "y2", "probability", "name"]
        )

        # If a person has been detected return the boxes with coordinates and the probability and the image_File id.w
        if 'person' in labels_class:
            human_score = bboxes[:, -1][0].tolist()
            boxes.append(
                ClassifiedBox(
                    x1=bboxes[labels_impt][0][0].tolist(), x2=bboxes[labels_impt][0][2].tolist(),
                    y1=bboxes[labels_impt][0][1].tolist(), y2=bboxes[labels_impt][0][3].tolist(),
                    probability=human_score, name="person"
                )
            )
        return {"media_id": image_id,
                "boxes": boxes}