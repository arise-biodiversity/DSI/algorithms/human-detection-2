import math
import os
import sys
import pandas as pd

from matplotlib import pyplot as plt
from sklearn.metrics import f1_score
from models.ensemble.Ensemble import Ensemble


class Test:
    def __init__(self, directory, trained_model=False):
        self.model = Ensemble(trained_model=trained_model)
        self.directory = directory

    def test_arise_output(self):
        """
        Method for testing the MergedModels class.
        (The f1 score of the model will be printed in the console)
        :return:
        """

        correct_labels = '../../data/test/csv/twente_labels.csv'
        images = os.listdir(self.directory)

        # Remove the .gitignore file otherwise model will count it too.
        images.remove('.gitignore')

        results_array = []
        for i, image_dir in enumerate(images):
            im = plt.imread(self.directory + "/" + image_dir)
            result = self.model.arise_output(im, image_dir)

            csv_data = pd.read_csv(correct_labels)
            current_image_data = csv_data[csv_data["mediaID"].str.strip() + '.jpg' == image_dir]
            if not current_image_data.empty:
                predicted = 0 if len(result["boxes"]) == 0 else 1
                ground_truth = current_image_data["ground_truth"].values[0]
                results_array.append({
                    "imageID": image_dir,
                    "ground_truth": ground_truth,
                    "human_detected": predicted
                })

            percentage_done = (i + 1) / len(images) * 100
            progress_bar = math.floor(percentage_done / 10)

            if i % 100 == 0 and len(results_array) != 0:
                dataframe = pd.DataFrame(data=results_array)
                sys.stdout.write(
                    f"\rImage {i + 1}/{len(images)} \t "
                    f"F1 score: {f1_score(dataframe['ground_truth'], dataframe['human_detected'])} \n"
                )
            else:
                sys.stdout.write(
                    f"\rProgress [{'=' * progress_bar}{' ' * (10 - progress_bar)}] {math.floor(percentage_done)}% \t "
                    f"Image {i + 1}/{len(images)}"
                )
            sys.stdout.flush()
        return results_array

    def write_csv(self, data):
        df = pd.DataFrame(data=data)
        print("\n\nF1 score:", f1_score(df['ground_truth'], df['human_detected']))
        df.to_csv('../../data/test/csv/ensemble.csv', index=False)


if __name__ == '__main__':
    directory = "../../data/test/images"

    # Set trained_model to 'False' if you want to use pre-trained model.
    # Set trained_model to 'True' if you want to use our fine-tuned model.
    model = Test(directory=directory, trained_model=True)
    data = model.test_arise_output()
    model.write_csv(data=data)
