import sys

import pandas as pd
from sklearn.metrics import accuracy_score, f1_score
from sklearn import naive_bayes
from sklearn.model_selection import train_test_split
import numpy as np

csv_dir = '../../data/CSV_data/merged/merged_optimal_condition.csv'


def get_csv():
    detectron2_data = pd.read_csv('../../data/CSV_data/detectron2/detectron2_results.csv')
    mmdetection_data = pd.read_csv('../../data/CSV_data/mmdetection/mmdetection_normal.csv')
    results = []

    for i, image in enumerate(detectron2_data['mediaID']):
        detectron2_row = detectron2_data[
            (detectron2_data["mediaID"].str.strip() == image) |
            (detectron2_data["mediaID"].str.strip() == image.split('.')[0])
            ]

        mmdetection_row = mmdetection_data[
            (mmdetection_data["id"].str.strip() == image) |
            (mmdetection_data["id"].str.strip() == image.split('.')[0])
            ]

        if not detectron2_row.empty and not mmdetection_row.empty:
            ground_truth = detectron2_row['ground_truth'].values[0]
            detectron2_conf = detectron2_row['human_score'].values[0]
            mmdetection_conf = mmdetection_row['accuracy']
            mmdetection_conf = 0 if mmdetection_conf.isna().any() else mmdetection_conf.values[0]

            results.append({
                'imageID': image,
                'ground_truth': ground_truth,
                'detectron2_conf': detectron2_conf,
                'mmdetection_conf': mmdetection_conf,
                'human_detected': 0
            })

    results = pd.DataFrame(data=results,
                           columns=['imageID', 'ground_truth', 'detectron2_conf', 'mmdetection_conf', 'human_detected'])
    results.to_csv(csv_dir, index=False)


def check_score_for_condition(random_state):
    data = pd.read_csv(csv_dir)

    x_data = data[['detectron2_conf', 'mmdetection_conf']]
    y_data = data['ground_truth']
    x_train, x_test, y_train, y_test = train_test_split(x_data, y_data, test_size=0.2, random_state=random_state)

    final_model = naive_bayes.GaussianNB(var_smoothing=0.17)
    final_model.fit(x_train, y_train)
    prediction = final_model.predict(x_test)

    y_true = y_test
    y_pred = prediction
    return f1_score(y_true=y_true, y_pred=y_pred), accuracy_score(y_true=y_true, y_pred=y_pred)


f1_array = []
acc_array = []
max_index = 1000
for i in range(max_index):
    f1, acc = check_score_for_condition(i + 1)
    f1_array.append(f1)
    acc_array.append(acc)
    sys.stdout.write(f"\rindex: {i + 1}/{max_index}")
    sys.stdout.flush()

print(f"\nF1 score mean: {str(np.mean(f1_array))}")
print(f"Accuracy score mean: {np.mean(acc_array)}")
print(f"F1 score min: {np.min(f1_array)} \t max: {np.max(f1_array)}")
print(f"Accuracy score min: {np.min(acc_array)} \t max: {np.max(acc_array)}")
