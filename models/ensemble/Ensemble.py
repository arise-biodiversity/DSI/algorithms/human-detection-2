import pandas as pd

from models.detectron2.Detectron2 import Detectron2
from models.mmdetection.Mmdetection import Mmdetection


class Ensemble:
    def __init__(self, docker_env=False, trained_model=False):
        self.models = {"detectron2": Detectron2(trained_model=trained_model), "mmdetection": Mmdetection(docker_env=docker_env)}
        self.final_model_input_features = [
            'detectron2_detect_count',
            'detectron2_max_score',
            'mmdetection_detect_count',
            'mmdetection_max_score'
        ]

    def arise_output(self, image, image_dir):
        """
        Method for checking if an image has a human in it or not
        :param image: The image that is being checked for humans (plt.imread()).
        :param image_dir: The name of the image (so this is the same as imageID).
        :return: An output for the arise model
        """
        final_output, decider_model_input = self.models_output(image, image_dir)

        model_input = pd.DataFrame(data=[decider_model_input])

        model_input = model_input[self.final_model_input_features]

        conf_condition = 0.8
        if (model_input['detectron2_max_score'].values[0] + model_input['mmdetection_max_score'].values[0]) < conf_condition:
            final_output['boxes'] = []

        return final_output

    def models_output(self, image, image_dir):
        """
        Method for getting the output of detectron 2 and mmdetection (so this is for getting an opinion of both models).
        :param image: The image that is being checked for humans (plt.imread()).
        :param image_dir: The name of the image (so this is the same as imageID).
        :return: An output for the arise model and an output that is used as input for the final model.
        """
        boxes = []
        human_score_array = []
        decider_model_input = {
            "detectron2_detect_count": 0,
            "detectron2_max_score": 0,
            "mmdetection_detect_count": 0,
            "mmdetection_max_score": 0
        }
        for model in self.models:
            this_model_data = self.models[model].arise_output(image, image_dir)
            model_conf = 0
            humans_count = 0
            for model_boxes in this_model_data["boxes"]:
                boxes.append(model_boxes)
                conf = model_boxes[4]  # Index for probability.
                human_score_array.append(conf)
                model_conf = max(model_conf, conf)
                humans_count += 1
            decider_model_input["imageID"] = image_dir
            decider_model_input[model + '_detect_count'] = humans_count
            decider_model_input[model + '_max_score'] = model_conf
        return {"media_id": image_dir, "boxes": boxes}, decider_model_input
