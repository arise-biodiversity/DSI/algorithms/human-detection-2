import os
from detectron2.utils.logger import setup_logger
from detectron2.engine import DefaultTrainer
from detectron2.evaluation import COCOEvaluator
from detectron2.data.datasets import register_coco_instances

from models.detectron2.Detectron2 import Detectron2


class Train:
    """This method trains the Detectron2 model with a custom COCO dataset.
    The method needs a directory path for the train_images and a path to the coco_file"""
    def __init__(self, directory, coco_file):
        self.model = Detectron2()
        self.cfg = self.model.cfg
        self.directory = directory
        self.coco_file = coco_file
        self.setup_config()

    def setup_config(self):
        self.cfg.DATASETS.TRAIN = ("images_train",)
        self.cfg.DATASETS.TEST = ()

        # These parameters need to be changed according to power of system.
        # Some parameters when changed are too heavy for CPU users.
        self.cfg.DATALOADER.NUM_WORKERS = 4
        self.cfg.SOLVER.IMS_PER_BATCH = 4
        self.cfg.SOLVER.BASE_LR = 0.001
        self.cfg.SOLVER.WARMUP_ITERS = 10
        self.cfg.SOLVER.MAX_ITER = 5  # adjust up if val mAP is still rising, adjust down if overfit
        self.cfg.SOLVER.STEPS = (100, 150)
        self.cfg.SOLVER.GAMMA = 0.05
        self.cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.5  # set threshold for this model

    def fit(self):
        register_coco_instances("images_train", {}, self.coco_file,
                                self.directory)
        setup_logger()
        self.setup_config()

        os.makedirs(self.cfg.OUTPUT_DIR, exist_ok=True)
        trainer = CocoTrainer(self.cfg)
        trainer.resume_or_load(resume=False)
        trainer.train()


class CocoTrainer(DefaultTrainer):
    @classmethod
    def build_evaluator(cls, cfg, dataset_name, output_folder=None):
        if output_folder is None:
            os.makedirs("coco_eval", exist_ok=True)
            output_folder = "coco_eval"

        return COCOEvaluator(dataset_name, cfg, False, output_folder)


if __name__ == '__main__':
    # Change this coco file and images directory if needed.
    coco_file = "../../data/train/_annotations.coco.json"
    directory = "../../data/train/images"

    model = Train(directory=directory, coco_file=coco_file)
    model.fit()

