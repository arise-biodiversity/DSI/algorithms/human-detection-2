import os
from collections import namedtuple
from detectron2.utils.logger import setup_logger
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.data import MetadataCatalog
from detectron2.utils.visualizer import Visualizer
from matplotlib import pyplot as plt

from utils import config


class Detectron2:
    """This class inits the Detectron2 model with default config.
    The arise_output is the method which will return the ARISE format"""
    def __init__(self, trained_model=False):
        setup_logger()
        self.cfg = get_cfg()

        self.cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"))

        # This enables CPU if DEVICE is set to CPU. Default = GPU
        if config.DEVICE == 'cpu':
            self.cfg.MODEL.DEVICE = 'cpu'

        # If you want to use the trained model.
        if trained_model:
            print("\n\n\nRunning the detectron2 model on trained model!!\n\n\n")
            self.cfg.MODEL.WEIGHTS = os.path.join(self.cfg.OUTPUT_DIR, "model_final.pth")
        else:
            print("\n\n\nRunning the detectron2 model on Pretrained model!!\n\n\n")
            self.cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(
                "COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml")

        self.cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.3  # set threshold for this model
        self.predictor = DefaultPredictor(self.cfg)

    def arise_output(self, image, image_dir):
        """This method puts an image into the algorithm and retrieves the predicted boxes and score.
        It returns the boxes and score in a partially ARISE json format."""
        # Get the predictions.
        outputs = self.predictor(image)

        # Get the instances.
        instances = outputs["instances"]

        # The detected classes indexes.
        detected_class_indexes = instances.pred_classes

        # The detected boxes.
        prediction_boxes = instances.pred_boxes

        # Get the classes that are in the detectron2 model.
        metadata = MetadataCatalog.get(self.cfg.DATASETS.TRAIN[0])
        class_catalog = metadata.thing_classes

        # Store results.
        human_score = 0
        boxes = []

        ClassifiedBox = namedtuple(
            "ClassifiedBox", ["x1", "y1", "x2", "y2", "probability", "name"]
        )

        if len(prediction_boxes) > 0:
            for idx, coordinates in enumerate(prediction_boxes):
                # The class index.
                class_index = detected_class_indexes[idx]

                # The name of the class.
                class_name = class_catalog[class_index]

                # The score of the prediction.
                class_score = instances.scores[idx].item()

                # If there is a person do the logic that is needed.
                if class_name == "person":
                    # Retrieve the score.
                    human_score = max(human_score, class_score)

                    # Get the coordinates of the box, so it can be stored in ARISE format.
                    person_box_coords = coordinates
                    boxes.append(
                        ClassifiedBox(
                            x1=person_box_coords[0].item(), x2=person_box_coords[1].item(),
                            y1=person_box_coords[2].item(), y2=person_box_coords[3].item(),
                            probability=human_score, name="person"
                        )
                    )

        return {"media_id": image_dir,
                "boxes": boxes}

    def show_image(self, img_path):
        """This method is to showcase the found image."""
        fig_width = 32
        fig_height = 18
        plt.figure(figsize=(fig_width, fig_height))
        im = plt.imread("../../data/test/images/" + img_path)
        outputs = self.predictor(im)
        v = Visualizer(im[:, :, ::-1], MetadataCatalog.get(self.cfg.DATASETS.TRAIN[0]), scale=1.2)
        out = v.draw_instance_predictions(outputs["instances"].to("cpu"))
        plt.axis("off")
        plt.imshow(out.get_image()[:, :, ::-1])
        plt.show()