# import some common libraries
import csv
import math
import os
import sys
import pandas as pd
import matplotlib.pyplot as plt

from detectron2.data import MetadataCatalog
from detectron2.engine import DefaultPredictor
from detectron2.utils.logger import setup_logger
from models.detectron2.Detectron2 import Detectron2

# Setup detectron2 logger
setup_logger()


class Test:
    """This class tests the Detectron2 model.
    You can decide whether to test with custom trained model or default model of Detectron2
    It saves the results to a csv."""

    def __init__(self, trained_model=False):
        self.model = Detectron2(trained_model)
        self.cfg = self.model.cfg

        # init predictor.
        self.predictor = DefaultPredictor(self.cfg)

    def predict(self, show_images=False):
        results_all = []

        # Own scoreboard to keep track of progress.
        correct_guesses = 0
        wrong_guesses = 0
        undefined_guesses = 0

        # Retrieve the images.
        images = os.listdir("../../data/test/images")

        # Remove the .gitignore file otherwise model will count it too.
        images.remove('.gitignore')

        # Read the csv of twente_labels to loop through same images.
        human_data = pd.read_csv("../../data/test/csv/twente_labels.csv")

        # Retrieve record.
        human_data["mediaID"] = human_data["mediaID"].str.strip() + '.jpg'

        # Loop through images
        for i, image in enumerate(images):
            # Check if record has data or not otherwise skip it and add +1 to unlabeled.
            image_data = human_data[human_data["mediaID"] == image]
            if not image_data.empty:
                # Read the img.
                im = plt.imread("../../data/test/images/" + image)

                # Predict the image.
                outputs = self.predictor(im)
                instances = outputs["instances"]
                detected_class_indexes = instances.pred_classes
                prediction_boxes = instances.pred_boxes

                # retrieve the known class names of detectron.
                metadata = MetadataCatalog.get(self.cfg.DATASETS.TRAIN[0])
                class_catalog = metadata.thing_classes

                # Values to track score of prediction and how many humans detected.
                human_score = 0
                human_detected = 0
                ground_truth = image_data['ground_truth'].values[0]

                # If there are prediction_boxes this means there is human detected. Retrieve scores.
                if len(prediction_boxes) <= 0:
                    pass
                else:
                    for idx, _ in enumerate(prediction_boxes):
                        class_index = detected_class_indexes[idx]
                        class_name = class_catalog[class_index]
                        class_score = instances.scores[idx].item()
                        # If there is a person name in the class add human count +1
                        if class_name == "person":
                            human_detected = 1
                            human_score = max(human_score, class_score)
                            if show_images:
                                self.model.show_image(img_path=image)

                # Create the row structure for csv.
                image_dict = {
                    "mediaID": image,
                    "human_score": human_score,
                    "human_detected": human_detected,
                    "ground_truth": ground_truth
                }

                # Apppend the found record.
                results_all.append(image_dict)

                # If the guess equals to the ground_truth, increase the correct_guesses by 1.
                if human_detected == ground_truth:
                    correct_guesses += 1
                else:
                    wrong_guesses += 1

            else:
                undefined_guesses += 1

            # Calculate the accuracy of the model.
            accuracy = 0
            if correct_guesses > 0 or wrong_guesses > 0:
                accuracy = correct_guesses / (correct_guesses + wrong_guesses)

            # Progressbar for terminal progress.
            percentage_done = (i + 1) / len(images) * 100
            progress_bar = math.floor(percentage_done / 10)

            # Print the results for every 100 lines.
            if i % 100 == 0:
                sys.stdout.write(
                    f"\rImage {i}/{len(images)} \t "
                    f"Correct: {correct_guesses} \t "
                    f"Incorrect: {wrong_guesses} \t "
                    f"Accuracy: {round(accuracy, 2)} \n"
                )
                sys.stdout.flush()
            else:
                sys.stdout.write(
                    f"\rProgress [{'=' * progress_bar}{' ' * (10 - progress_bar)}] {round(percentage_done)}% \t "
                    f"Image {i + 1}/{len(images)} \t "
                    f"Correct answers = {correct_guesses} \t "
                    f"Incorrect answers = {wrong_guesses} \t "
                    f"Without label = {undefined_guesses} \t "
                    f"Accuracy = {round(accuracy, 2)}"
                )
                sys.stdout.flush()
        return results_all


def create_csv_from_dict(data, file_name):
    keys = data[0].keys()
    with open('../../data/test/csv/' + file_name, 'w', newline='') as output_csv:
        csv_writer = csv.DictWriter(output_csv, fieldnames=keys)
        csv_writer.writeheader()
        csv_writer.writerows(data)


if __name__ == '__main__':
    # Put trained-model to TRUE if you want to use our trained model.
    trained_model = False
    filename = None

    # Set this to True if you want to showcase the images with humans predicted.
    show_images = False

    if trained_model:
        filename = "detectron2_trainedxx.csv"
    else:
        filename = "detectron2xx.csv"

    model = Test(trained_model=trained_model) # Change this boolean above.
    results = model.predict(show_images=show_images) # Change this boolean above.

    # Only write if there are results
    if len(results) > 0:
        create_csv_from_dict(results, filename)
